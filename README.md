## OUBuild
This is a fork of the the open source version of MIT's Scratch 2.0 found here https://github.com/LLK/scratch-flash and the core code for the official MIT version found on http://scratch.mit.edu. MIT has released their Scratch 2.0 code under the GPL version 2 license. This means that forks can be released under the GPL v2 or any later version of the GPL.

### Building
Get Intellij IDE (Trial one's OK).

Get Apache Flex SDK 4.15 and install it (note where you install it as you'll need it below).

Clone repo, open IDE, select 'Open' and navigate to repos' top-level folder (it will have .idea file in it ready to load up as a project).

Edit project's build configuration to point at the location of your Flex SDK. Click on small screen icon labelled 'scratch (app)' in the bottom-right corner of the IDE. Click 'Project structure' > 'Dependencies' and then select SDK's top-level folder.

To debug click Run / Debug.

To build click Build / 'Package Air Application' go for type 'installer *.air'.

To Deploy get the file 'scratch-flash-master.air' from the folder out/production/scratch/ and distribute it to users who just need to click on it to installer the application.