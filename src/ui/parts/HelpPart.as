/*
 * Scratch Project Editor and Player
 * Copyright (C) 2014 Massachusetts Institute of Technology
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

// TopBarPart.as
// John Maloney, November 2011
//
// This part holds the Scratch Logo, cursor tools, screen mode buttons, and more.

package ui.parts {

import assets.Resources;
import flash.display.*;
import flash.events.MouseEvent;
import flash.events.Event;
import flash.html.__HTMLScriptArray;
import flash.text.*;
import uiwidgets.*;
import flash.html.HTMLLoader;
import flash.net.URLRequest;
import translation.TranslatableStrings;
import com.greensock.TweenLite;

public class HelpPart extends UIPart {
	private var helpScreens:Object = {
		"call": "blocks/usedefine.html",
		"procDef": "blocks/define.html",
		"forward:":"blocks/move-steps.html",
		"turnRight:":"blocks/turn-right.html",
		"turnLeft:":"blocks/turn-left.html",
		"heading:":"blocks/point-direction.html",
		"pointTowards:":"blocks/point-towards.html",
		"gotoX:y:":"blocks/go-to-xy.html",
		"gotoSpriteOrMouse:":"blocks/go-to.html",
		"glideSecs:toX:y:elapsed:from:":"blocks/glide.html",
		"changeXposBy:":"blocks/change-x.html",
		"xpos:":"blocks/set-x.html",
		"changeYposBy:":"blocks/change-y.html",
		"ypos:":"blocks/set-y.html",
		"bounceOffEdge":"blocks/if-on-edge-bounce.html",
		"setRotationStyle":"blocks/setrotation.html",
		"xpos":"blocks/x-position.html",
		"ypos":"blocks/y-position.html",
		"heading":"blocks/direction.html",
		"say:duration:elapsed:from:":"blocks/say-for-seconds.html",
		"say:":"blocks/say.html",
		"think:duration:elapsed:from:":"blocks/think-for-seconds.html",
		"think:":"blocks/think.html",
		"show":"blocks/show.html",
		"hide":"blocks/hide.html",
		"lookLike:":"blocks/switch-costume.html",
		"nextCostume":"blocks/next-costume.html",
		"nextScene":"blocks/nextbackdrop.html",
		"startScene":"blocks/switch-backdrop.html",
		"startSceneAndWait":"blocks/switch-backdrop-and-wait.html",
		"changeGraphicEffect:by:":"blocks/change-effect.html",
		"setGraphicEffect:to:":"blocks/set-effect.html",
		"filterReset":"blocks/clear-graphic-effects.html",
		"changeSizeBy:":"blocks/change-size.html",
		"setSizeTo:":"blocks/set-size.html",
		"comeToFront":"blocks/go-to-front.html",
		"goBackByLayers:":"blocks/go-back-layer.html",
		"costumeIndex":"blocks/costume-number.html",
		"sceneName":"blocks/backdrop-name.html",
		"backgroundIndex":"blocks/backdrop-number.html",
		"scale":"blocks/size.html",
		"playSound:":"blocks/playsound.html",
		"doPlaySoundAndWait":"blocks/playsound_untildone.html",
		"stopAllSounds":"blocks/stopsound.html",
		"playDrum":"blocks/play_drum.html",
		"rest:elapsed:from:":"blocks/rest.html",
		"noteOn:duration:elapsed:from:":"blocks/playnote.html",
		"instrument:":"blocks/setinstrument.html",
		"changeVolumeBy:":"blocks/change-volume.html",
		"setVolumeTo:":"blocks/set-volume.html",
		"changeTempoBy:":"blocks/change-tempo.html",
		"setTempoTo:":"blocks/set-tempo.html",
		"tempo":"blocks/tempo.html",
		"volume":"blocks/volume.html",
		"clearPenTrails":"blocks/clear.html",
		"stampCostume":"blocks/stamp.html",
		"putPenDown":"blocks/pendown.html",
		"putPenUp":"blocks/penup.html",
		"penColor:":"blocks/setpencolor.html",
		"changePenHueBy:":"blocks/changecolor.html",
		"setPenHueTo:":"blocks/setpencolorto.html",
		"changePenShadeBy:":"blocks/changeshadeby.html",
		"setPenShadeTo:":"blocks/setshade.html",
		"changePenSizeBy:":"blocks/changepensize.html",
		"penSize:":"blocks/setpensize.html",
		"makeVariable":"blocks/make-a-variable.html",
		"variable":"blocks/variable.html",
		"setVar:to:":"blocks/set-variable.html",
		"readVariable":"blocks/change-variable.html",
		"changeVar:by:":"blocks/change-variable.html",
		"showVariable:":"blocks/show-variable.html",
		"hideVariable:":"blocks/hide-variable.html",
		"makeList":"blocks/make-a-list.html",
		"list":"blocks/list.html",
		"append:toList:":"blocks/add-to-list.html",
		"deleteLine:ofList:":"blocks/delete-from-list.html",
		"insert:at:ofList:":"blocks/insert-list.html",
		"setLine:ofList:to:":"blocks/replace-list.html",
		"getLine:ofList:":"blocks/item-of-list.html",
		"lineCountOfList:":"blocks/length-of-list.html",
		"list:contains:":"blocks/list-contains.html",
		"showList:":"blocks/show-list.html",
		"hideList:":"blocks/hide-list.html",
		"whenGreenFlag":"blocks/when-flag-clicked.html",
		"whenKeyPressed":"blocks/when-key-pressed.html",
		"whenClicked":"blocks/when-this-sprite-clicked.html",
		"whenSceneStarts":"blocks/when-backdrop-switches.html",
		"whenSensorGreaterThan":"blocks/when-loudness.html",
		"whenIReceive":"blocks/when-i-receive.html",
		"broadcast:":"blocks/broadcast.html",
		"doBroadcastAndWait":"blocks/broadcast-wait.html",
		"wait:elapsed:from:":"blocks/wait-secs.html",
		"doRepeat":"blocks/repeat.html",
		"doForever":"blocks/forever.html",
		"doIf":"blocks/if.html",
		"doIfElse":"blocks/if-else.html",
		"doWaitUntil":"blocks/wait-until.html",
		"doUntil":"blocks/repeat-until.html",
		"stopScripts":"blocks/stop.html",
		"whenCloned":"blocks/clone-startup.html",
		"createCloneOf":"blocks/create-clone.html",
		"deleteClone":"blocks/delete-this-clone.html",
		"touching:":"blocks/touching.html",
		"touchingColor:":"blocks/touching-color.html",
		"color:sees:":"blocks/color-is-touching.html",
		"distanceTo:":"blocks/distance-to.html",
		"doAsk":"blocks/ask-and-wait.html",
		"answer":"blocks/answer.html",
		"keyPressed:":"blocks/key-pressed.html",
		"mousePressed":"blocks/mouse-down.html",
		"mouseX":"blocks/mouse-x.html",
		"mouseY":"blocks/mouse-y.html",
		"soundLevel":"blocks/loudness.html",
		"senseVideoMotion":"blocks/video-motion.html",
		"setVideoState":"blocks/turn-video.html",
		"setVideoTransparency":"blocks/set-video-transparency.html",
		"timer":"blocks/timer.html",
		"timerReset":"blocks/reset-timer.html",
		"getAttribute:of:":"blocks/getattribute.html",
		"timeAndDate":"blocks/current-time.html",
		"timestamp":"blocks/timestamp.html",
		"getUserName":"blocks/username.html",
		"+":"blocks/add.html",
		"-":"blocks/subtract.html",
		"*":"blocks/multiply.html",
		"/":"blocks/divide.html",
		"randomFrom:to:":"blocks/pick-random.html",
		"<":"blocks/less-than.html",
		"=":"blocks/equal.html",
		">":"blocks/greater-than.html",
		"&":"blocks/and.html",
		"|":"blocks/or.html",
		"not":"blocks/not.html",
		"concatenate:with:":"blocks/join.html",
		"letter:of:":"blocks/letter-of.html",
		"stringLength:":"blocks/length-of.html",
		"%":"blocks/mod.html",
		"rounded":"blocks/round.html",
		"computeFunction:of:":"blocks/computefunction.html",
		"makeBlock":"blocks/make-a-block.html",
		"addExtension":"blocks/add-an-extension.html",
        "home":"home.html"};
	private var shape:Shape;
	private var openButton:IconButton;
    private var closeButton:IconButton;
	private var homeButton:IconButton;
	private var helpPartOffSet:Number = 10;
	private var helpWindowOffset:Number = 30;

	private var helpWindow:HelpFrame;
    private var html:HTMLLoader = new HTMLLoader();
	public var is_open:Boolean = false;
	public var open_x:Number = 350;
	public var closed_x:Number = 20;
	private var opened_once:Boolean = false;

	public function HelpPart(app:Scratch) {
		this.app = app;

		this.shape = new Shape();

		addChild(this.shape = new Shape());
		addChild(this.openButton = new IconButton(open, 'helpTool'));
		addChild(this.homeButton = new IconButton(home, 'home'));
        addChild(this.closeButton = new IconButton(close, 'close'));
		this.closeButton.visible = false;

		this.helpWindow = new HelpFrame(true);
		this.html.addEventListener(Event.COMPLETE, onHtmlLoader_COMPLETE);
		this.loadHtmlPage("home");

		this.helpWindow.x = 20;
		this.helpWindow.y = 30;

		addChild(this.helpWindow);
	}

	public function setWidthHeight(w:int, h:int):void {
		var g:Graphics = this.shape.graphics;
		g.clear();
		g.beginFill(CSS.panelColor);
		g.drawRect(0, 0, w, h - (this.y + this.helpPartOffSet));
		g.endFill();

        this.homeButton.x = 320;
        this.homeButton.y = 5;
        this.openButton.y = 4;

        this.closeButton.y = 7;
        this.closeButton.x = 7;

		this.html.width = 340;
		this.html.height = h - (this.y + this.helpWindow.y + this.helpWindowOffset);
		this.helpWindow.setWidthHeight(340, h - (this.y + this.helpWindow.y + this.helpWindowOffset));
	}

	/*
	Loads the page into the Help system.
	@param page String The name of the page to load
	 */
	public function loadHtmlPage(page:String):void {
        var urlReq:URLRequest;
		var helpPage:String = this.helpScreens[page];

		urlReq = new URLRequest("app:/assets/help/en/" + helpPage);

        this.html.load(urlReq);
	}

	public function close(ignore:* = null):void {
		this.closeButton.visible = false;
		this.closeButton.alpha = 0;
		this.openButton.visible = true;
		this.is_open = false;
		TweenLite.to(this, 0.5, {x:stage.stageWidth - this.closed_x});
        TweenLite.to(this.openButton, 1, {alpha: 1});
	}

    public function open(ignore:* = null):void {
        var open_position:Number = stage.stageWidth - this.open_x;
		this.openButton.visible = false;
		this.openButton.alpha = 0;
		this.closeButton.visible = true;
		this.is_open = true;
		TweenLite.to(this, 1, {x:open_position});
        TweenLite.to(this.closeButton, 1, {alpha: 1});
    }

	public function home(ignore:* = null):void {
		this.loadHtmlPage("home");
	}

	protected function onHtmlLoader_COMPLETE(event:Event):void {
        var helpPane: ScrollFrameContents = new ScrollFrameContents();
        helpPane.addChild(this.html);
        helpPane.updateSize();

        this.helpWindow.setContents(helpPane);

		if (this.opened_once) {
            this.open();
		} else {
			this.opened_once = true;
		}
	}
}
}
